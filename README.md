To launch backend part simply launch class : 
`com.eshop.EShopApplication`

To build project :
`mvn clean install`

Before you start test API get Jwt token
Hit URL
    POST http://localhost:8080/auth
    {"username":"admin@gmail.com", "password":"admin"}

You'll get token in response. Put this token to header
    `Authorization` and aftes you'll have access to all API.

Working URL's:

Category:

1) Save:

    POST http://localhost:8080/category/save
    {"name":"clothes"}
    
2) Update:

    PUT http://localhost:8080/category/1
    {"name":"clothesElite"}
    
3) Delete:
    
     DELETE http://localhost:8080/category/1
     
4) Get:
    
     GET http://localhost:8080/category/1

5) Get category products:
    
    GET http://localhost:8080/category/products/category/1/currency/USD
    

Product:

1) Save
        
      POST  http://localhost:8080/product/
      {"name":"Hat", "price":45.56, "categoryId" : 1, "currency":"USD"}
      
2) Update:

    PUT http://localhost:8080/product/1
    {"name":"Hat", "price":40.23, "categoryId" : 1, "currency":"USD"}
    
3) Delete:
    
     DELETE http://localhost:8080/product/category/1/product/1
       
4) Get:
    
     GET http://localhost:8080/product/1/currency/USD
      
      