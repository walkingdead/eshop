package com.eshop.security.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "USERS")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "USERS_ID")
    private Long id;

    @Column(name = "USER_EMAIL")
    @NotNull(message = "email can't be null")
    @NotBlank(message = "email is required")
    @Email(message = "invalid email")
    private String email;

    @Column(name = "USER_PASSWORD")
    @NotNull(message = "password can't be null")
    @NotBlank(message = "password is required")
    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(name = "USERS_AUTHORITY", joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "USERS_ID")},
            inverseJoinColumns = {@JoinColumn(name = "AUTHORITY_ID", table = "authority", referencedColumnName = "AUTHORITY_ID")})
    @NotNull(message = "authorities can't be null")
    @NotEmpty(message = "authorities is required")
    private Set<Authority> authorities;

    public User() {
    }

    public User(String login, String password, String email, Set<Authority> authorities) {
        this.password = password;
        this.email = email;
        this.authorities = authorities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 83)
                .append(id)
                .append(email)
                .append(authorities)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != User.class) {
            return false;
        }
        User other = (User) obj;

        return new EqualsBuilder()
                .append(this.id, other.id)
                .append(this.email, other.email)
                .append(this.authorities, other.authorities)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("email", email)
                .append("authorities", authorities)
                .toString();
    }
}
