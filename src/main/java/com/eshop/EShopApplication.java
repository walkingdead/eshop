package com.eshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class EShopApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(EShopApplication.class, args);
    }
}
