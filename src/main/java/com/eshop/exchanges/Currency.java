package com.eshop.exchanges;


import com.eshop.exchanges.exception.CurrencyNotFoundException;
import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum Currency {

    AUD("AUD"),
    BGN("BGN"),
    BRL("BRL"),
    CAD("CAD"),
    CHF("CHF"),
    CNY("CNY"),
    CZK("CZK"),
    DKK("DKK"),
    GBP("GBP"),
    HKD("HKD"),
    HRK("HRK"),
    HUF("HUF"),
    IDR("IDR"),
    ILS("ILS"),
    INR("INR"),
    JPY("JPY"),
    KRW("KRW"),
    MXN("MXN"),
    MYR("MYR"),
    NOK("NOK"),
    NZD("NZD"),
    PHP("PHP"),
    PLN("PLN"),
    RON("RON"),
    RUB("RUB"),
    SEK("SEK"),
    SGD("SGD"),
    THB("THB"),
    TRY("TRY"),
    USD("USD"),
    EUR("EUR"),
    ZAR("ZAR");

    private static final Map<String, Currency> CURRENCIES = Arrays.stream(Currency.values()).collect(Collectors.toMap(Currency::getName, Function.identity()));

    Currency(String name) {
        this.name = name;
    }

    private final String name;

    public String getName() {
        return name;
    }

    @JsonCreator
    public static Currency forValue(String value) {
        return Currency.valueOf(value);
    }

    public static Currency getCurrency(String currencyName) {
        if (!CURRENCIES.containsKey(currencyName)) {
            throw new CurrencyNotFoundException(String.format("Currency %s is not supported", currencyName));
        }
        return CURRENCIES.get(currencyName);
    }
}
