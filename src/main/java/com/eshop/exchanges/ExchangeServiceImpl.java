package com.eshop.exchanges;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ExchangeServiceImpl implements ExchangeService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${fixer.url}")
    private String fixerUrl;

    public CurrencyRate getCurrencyRate(Currency currency) {
        HttpEntity<String> currencyRateJson = restTemplate.exchange(fixerUrl + "?symbols={currencyName}",
                HttpMethod.GET, null, String.class, currency.getName());
        JsonElement root = new JsonParser().parse(currencyRateJson.getBody());
        JsonElement rates = root.getAsJsonObject().get("rates");
        return new CurrencyRate(currency,
                rates.getAsJsonObject().get(currency.getName()).getAsBigDecimal());
    }

}
