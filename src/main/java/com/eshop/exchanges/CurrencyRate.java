package com.eshop.exchanges;


import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

public class CurrencyRate {

    private Currency currency;
    private BigDecimal rate;

    public CurrencyRate() {
    }

    public CurrencyRate(Currency currency, BigDecimal rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("currency", currency)
                .append("rate", rate)
                .toString();
    }
}
