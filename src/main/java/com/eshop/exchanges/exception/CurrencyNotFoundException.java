package com.eshop.exchanges.exception;


public class CurrencyNotFoundException extends RuntimeException {

    public CurrencyNotFoundException(String msg) {
        super(msg);
    }

    public CurrencyNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}
