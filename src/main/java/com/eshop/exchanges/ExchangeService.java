package com.eshop.exchanges;

public interface ExchangeService {

    CurrencyRate getCurrencyRate(Currency currency);
}

