package com.eshop.product.exception;


public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(String msg) {
        super(msg);
    }

    public ProductNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}
