package com.eshop.product;

import com.eshop.category.Category;
import com.eshop.category.exception.CategoryNotFoundException;
import com.eshop.category.CategoryService;
import com.eshop.exchanges.Currency;
import com.eshop.exchanges.CurrencyRate;
import com.eshop.exchanges.ExchangeService;
import com.eshop.product.Product;
import com.eshop.product.ProductDto;
import com.eshop.product.exception.ProductNotFoundException;
import com.eshop.product.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ExchangeService exchangeService;

    public Long saveProduct(ProductDto productDto) {
        Category category = categoryService.getById(productDto.getCategoryId());
        if (category == null) throw new CategoryNotFoundException("Category not found");
        CurrencyRate currencyRate = exchangeService.getCurrencyRate(productDto.getCurrency());
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setCategory(category);
        product.setPrice(productDto.getPrice().divide(currencyRate.getRate(), BigDecimal.ROUND_HALF_DOWN));
        Product savedProduct  = productRepository.save(product);
        return savedProduct.getId();
    }

    public void deleteProduct(Long productId, Long categoryId) {
        Category category = categoryService.getById(categoryId);
        if (category == null) throw new CategoryNotFoundException("Category not found");
        Product product = productRepository.findOne(productId);
        if (product == null) throw new ProductNotFoundException("Product not found");
        category.getProducts().remove(product);
        categoryService.saveCategory(category);
    }

    public Product getProduct(Long productId, Currency currency) {
        Product product = productRepository.findOne(productId);
        CurrencyRate currencyRate = exchangeService.getCurrencyRate(currency);
        product.setPrice(currencyRate.getRate().multiply(product.getPrice()));
        return product;
    }

}
