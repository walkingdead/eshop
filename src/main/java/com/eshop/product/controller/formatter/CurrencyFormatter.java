package com.eshop.product.controller.formatter;


import com.eshop.exchanges.Currency;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class CurrencyFormatter implements Formatter<Currency> {
    @Override
    public Currency parse(String s, Locale locale) throws ParseException {
        return Currency.getCurrency(s);
    }

    @Override
    public String print(Currency currency, Locale locale) {
        return currency.toString();
    }
}
