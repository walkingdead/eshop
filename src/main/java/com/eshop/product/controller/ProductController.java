package com.eshop.product.controller;

import com.eshop.exchanges.Currency;
import com.eshop.product.Product;
import com.eshop.product.ProductDto;
import com.eshop.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;

@RestController
@RequestMapping("/product")
@Validated
@PreAuthorize("hasAuthority('ADMIN')")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity saveProduct(@RequestBody @Valid ProductDto productDto) {
        Long productId = productService.saveProduct(productDto);
        URI uri = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/product/get/{id}").build()
                .expand(productId).toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PUT)
    public ResponseEntity updateProduct(@PathVariable(value = "productId")
                                        @NotNull(message = "product id is required") Long productId,
                                        @RequestBody @Valid ProductDto productDto) {
        productDto.setId(productId);
        productService.saveProduct(productDto);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/category/{categoryId}/product/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteProduct(@PathVariable("categoryId")
                                        @NotNull(message = "category id is required") Long categoryId,
                                        @PathVariable("productId")
                                        @NotNull(message = "product id is required") Long productId) {
        productService.deleteProduct(productId, categoryId);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{productId}/currency/{currency}", method = RequestMethod.GET)
    public ResponseEntity getProductById(@PathVariable("productId")
                                         @NotNull(message = "product id is required") Long productId,
                                         @PathVariable("currency")
                                         @NotNull(message = "currency is required") Currency currency) {
        Product product = productService.getProduct(productId, currency);
        return product == null
                ? ResponseEntity.noContent().build()
                : ResponseEntity.ok(product);
    }

}
