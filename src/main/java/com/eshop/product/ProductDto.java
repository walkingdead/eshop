package com.eshop.product;


import com.eshop.exchanges.Currency;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ProductDto {

    private Long id;

    @NotBlank(message = "product name is required")
    private String name;
    @NotNull(message = "product price is required")
    private BigDecimal price;
    @NotNull(message = "product category is required")
    private Long categoryId;
    @NotNull(message = "product currency is required")
    private Currency currency;

    public ProductDto() {
    }

    public ProductDto(String name, BigDecimal price, Long categoryId, Currency currency) {
        this.name = name;
        this.price = price;
        this.categoryId = categoryId;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
