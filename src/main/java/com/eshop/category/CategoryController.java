package com.eshop.category;

import com.eshop.category.Category;
import com.eshop.category.CategoryService;
import com.eshop.exchanges.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;

@RestController
@RequestMapping("/category")
@Validated
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity saveCategory(@RequestBody @Valid Category category) {
        Long categoryId = categoryService.saveCategory(category);
        URI uri = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/category/get/{id}").build()
                .expand(categoryId).toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.PUT)
    public ResponseEntity updateCategory(@PathVariable(value = "categoryId")
                                         @NotNull(message = "category is required") Long categoryId,
                                         @RequestBody @Valid Category category) {
        category.setId(categoryId);
        categoryService.saveCategory(category);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCategory(@PathVariable("categoryId")
                                         @NotNull(message = "category is required") Long categoryId) {
        categoryService.deleteCategory(categoryId);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/products/category/{categoryId}/currency/{currency}", method = RequestMethod.GET)
    public ResponseEntity getCategoryProducts(@PathVariable("categoryId")
                                              @NotNull(message = "category is required") Long categoryId,
                                              @PathVariable("currency")
                                              @NotNull(message = "currency is required") Currency currency) {
        return ResponseEntity.ok(categoryService.getProducts(categoryId, currency));
    }

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public ResponseEntity getCategory(@PathVariable("categoryId")
                                      @NotNull(message = "category is required") Long categoryId) {
        return ResponseEntity.ok(categoryService.getById(categoryId));
    }
}
