package com.eshop.category.exception;


public class CategoryNotFoundException extends RuntimeException {

    public CategoryNotFoundException(String msg) {
        super(msg);
    }

    public CategoryNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}
