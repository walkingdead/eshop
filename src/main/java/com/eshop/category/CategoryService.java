package com.eshop.category;

import com.eshop.category.exception.CategoryNotFoundException;
import com.eshop.exchanges.Currency;
import com.eshop.exchanges.CurrencyRate;
import com.eshop.exchanges.ExchangeService;
import com.eshop.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
@Transactional
@PreAuthorize("hasAuthority('ADMIN')")
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ExchangeService exchangeService;

    public Long saveCategory(Category category) {
        Category savedCategory = categoryRepository.save(category);
        return savedCategory.getId();
    }

    public void deleteCategory(Long categoryId) {
        categoryRepository.delete(categoryId);
    }

    public Category getById(Long categoryId) {
        return categoryRepository.findOne(categoryId);
    }

    public Set<Product> getProducts(Long categoryId, Currency currency) {
        Category category = categoryRepository.findOne(categoryId);
        if (category == null) throw new CategoryNotFoundException("Category not found");
        Set<Product> products = category.getProducts();
        CurrencyRate currencyRate = exchangeService.getCurrencyRate(currency);
        products.forEach(product -> product.setPrice(currencyRate.getRate().multiply(product.getPrice())));
        return products;
    }
}
