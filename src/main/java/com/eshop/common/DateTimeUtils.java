package com.eshop.common;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;


public class DateTimeUtils {

    private DateTimeUtils() {
    }

    public static Date asDateTime(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
