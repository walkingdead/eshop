package com.eshop.common.constants;


public final class ErrorMessage {

    private ErrorMessage() {
    }

    public static final String DATA_BASE_ERROR = "DataBase Error";
    public static final String UNKNOWN_SERVER_ERROR = "Unknown Server Error";
}
