package com.eshop.common.interceptor;

import com.eshop.common.exception.DatabaseException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Aspect
@Component
public class EShopServiceInterceptor {

    @Around("handleExceptionPointcut()")
    public Object handleServiceExceptions(ProceedingJoinPoint pjp) throws Throwable {
        Object object;
        try {
            object = pjp.proceed();
        } catch (SQLException | DataAccessException e) {
            throw new DatabaseException("DataBase Error", e);
        }
        return object;
    }

    @Pointcut("within(*..*Service)")
    public void handleExceptionPointcut() {
    }
}
