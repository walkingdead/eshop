INSERT INTO USERS (USER_EMAIL, USER_PASSWORD)
VALUES ('admin@gmail.com', '$2a$10$0biIX9xckzgMCIrjwvo0XONr3TWrFRkODiiWcSldRKCuOD5plZvEm');

INSERT INTO AUTHORITY (AUTHORITY_NAME) VALUES ('ADMIN');

INSERT INTO USERS_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES ((SELECT USERS_ID
                                                             FROM USERS
                                                             WHERE USER_EMAIL = 'admin@gmail.com'),
                                                            (SELECT AUTHORITY_ID
                                                             FROM AUTHORITY
                                                             WHERE AUTHORITY_NAME = 'ADMIN'));