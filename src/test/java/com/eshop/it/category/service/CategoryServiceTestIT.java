package com.eshop.it.category.service;

import com.eshop.category.Category;
import com.eshop.category.CategoryRepository;
import com.eshop.category.CategoryService;
import com.eshop.common.annotation.IntegrationTestRunner;
import com.eshop.common.mappers.category.CategoryMapper;
import com.eshop.exchanges.Currency;
import com.eshop.exchanges.ExchangeService;
import com.eshop.product.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import static com.eshop.common.ScriptConstants.FILL_CATEGORY_SCRIPT;
import static com.eshop.common.ScriptConstants.FILL_PRODUCT_SCRIPT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

@IntegrationTestRunner
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@WithMockUser(authorities = "ADMIN")
public class CategoryServiceTestIT {

    @Autowired
    @Qualifier("testExchangeService")
    private ExchangeService exchangeService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void saveCategoryTest() {
        Category category = new Category(null, "Food", Collections.emptySet());
        Long categoryId = categoryService.saveCategory(category);

        assertThat("Category was created", categoryId, notNullValue());
        assertThat("Category id not null", categoryId, notNullValue());
    }

    @Test
    @Sql(scripts = FILL_CATEGORY_SCRIPT)
    public void deleteCategoryTest() {

        Category category = findApplicationById(1L);
        categoryService.deleteCategory(category.getId());
        categoryRepository.flush();
        boolean deletedCategoryExist = isCategoryExists(category.getId());

        assertThat("Category should be removed", deletedCategoryExist, is(false));
    }

    @Test
    @Sql(scripts = FILL_CATEGORY_SCRIPT)
    public void getCategoryByIdTest() {

        Category savedCategory = categoryService.getById(1L);

        assertThat("Good category should be found", savedCategory, notNullValue());
        assertThat("Category name should be goods", savedCategory.getName(), is("clothes"));
    }

    @Test
    @Sql(scripts = FILL_PRODUCT_SCRIPT)
    public void getCategoryProductsTest() {

        Set<Product> products = categoryService.getProducts(1L, Currency.USD);

        assertThat("Category goods has 3 product", products, hasSize(3));

        Set<String> productNames = products.stream().map(Product::getName).collect(Collectors.toSet());
        assertThat("Product_ names are", productNames, hasItems("hat", "shirt", "shoes"));
    }

    private static Category createCategory(String categoryName) {
        return new Category(null, categoryName, Collections.emptySet());
    }

    private Category findApplicationById(Long categoryId) {
        return jdbcTemplate.queryForObject("SELECT * FROM category WHERE category_id = ? ",
                new CategoryMapper(), categoryId);
    }

    private boolean isCategoryExists(Long categoryId) {
        return jdbcTemplate.query("SELECT * FROM category WHERE category_id = ?",
                new Object[]{categoryId}, new BeanPropertyRowMapper<>(Category.class)).size() != 0;
    }
}
