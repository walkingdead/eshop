package com.eshop.it.product.service;

import com.eshop.category.exception.CategoryNotFoundException;
import com.eshop.common.annotation.IntegrationTestRunner;
import com.eshop.common.mappers.product.ProductMapper;
import com.eshop.exchanges.Currency;
import com.eshop.exchanges.ExchangeService;
import com.eshop.product.Product;
import com.eshop.product.ProductDto;
import com.eshop.product.exception.ProductNotFoundException;
import com.eshop.product.ProductRepository;
import com.eshop.product.ProductService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static com.eshop.common.ScriptConstants.FILL_CATEGORY_SCRIPT;
import static com.eshop.common.ScriptConstants.FILL_PRODUCT_SCRIPT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@IntegrationTestRunner
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@WithMockUser(authorities = "ADMIN")
public class ProductServiceTestIT {

    @Autowired
    @Qualifier("testExchangeService")
    private ExchangeService exchangeService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @Sql(scripts = FILL_CATEGORY_SCRIPT)
    public void saveProductTest() {
        Long productId = productService.saveProduct(createProduct("pants", BigDecimal.valueOf(32.89)));

        assertThat("Product was created", productId, notNullValue());
        assertThat("Product id not null", productId, notNullValue());
    }

    @Test
    public void saveProductWithoutCategoryTest() {
        expectedException.expect(CategoryNotFoundException.class);
        expectedException.expectMessage("Category not found");
        productService.saveProduct(createProduct("pants", BigDecimal.valueOf(32.89)));
    }

    @Test
    @Sql(scripts = FILL_PRODUCT_SCRIPT)
    public void deleteProductTest() {

        Product product = findProductById(1L);
        productService.deleteProduct(product.getId(), product.getCategory().getId());
        productRepository.flush();
        boolean deletedProductExist = isProductExists(product.getId());

        assertThat("Product should be removed", deletedProductExist, is(false));
    }

    @Test
    @Sql(scripts = FILL_PRODUCT_SCRIPT)
    public void deleteProductWithNonExistingCategoryTest() {
        expectedException.expect(CategoryNotFoundException.class);
        expectedException.expectMessage("Category not found");
        Product product = findProductById(1L);
        productService.deleteProduct(product.getId(), 0L);
    }

    @Test
    @Sql(scripts = FILL_PRODUCT_SCRIPT)
    public void deleteNonExistingProductTest() {
        expectedException.expect(ProductNotFoundException.class);
        expectedException.expectMessage("Product not found");
        Product product = findProductById(1L);
        productService.deleteProduct(0L, product.getCategory().getId());
    }

    @Test
    @Sql(scripts = FILL_PRODUCT_SCRIPT)
    public void getProductTest() {
        Product product = productService.getProduct(2L, Currency.USD);

        assertThat("Product was found", product, notNullValue());
        assertThat("Product name", product.getName(), is("shirt"));
        assertThat("Product price", product.getPrice(), comparesEqualTo(BigDecimal.valueOf(31.152)));
    }

    private static ProductDto createProduct(String name, BigDecimal price) {
        return new ProductDto(name, price, 1L, Currency.USD);
    }

    private Product findProductById(Long productId) {
        return jdbcTemplate.queryForObject("SELECT * FROM product as p INNER JOIN product_category as pc " +
                        "ON p.product_id = pc.product_id INNER JOIN category as c ON c.category_id = pc.category_id " +
                        "where p.product_id = ?",
                new ProductMapper(), productId);
    }

    private boolean isProductExists(Long productId) {
        return jdbcTemplate.query("SELECT * FROM product WHERE product_id = ?",
                new Object[]{productId}, new BeanPropertyRowMapper<>(Product.class)).size() != 0;
    }
}
