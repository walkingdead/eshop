package com.eshop.unit.category.controller;

import com.eshop.category.CategoryController;
import com.eshop.category.Category;
import com.eshop.category.CategoryService;
import com.eshop.common.utils.ControllerTestHelper;
import com.eshop.exchanges.Currency;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(CategoryController.class)
@WithMockUser
public class CategoryControllerValidationTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    private ControllerTestHelper controllerHelper;

    @Before
    public void setup() throws Exception {
        mvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
        controllerHelper = new ControllerTestHelper(mvc);
    }

    @MockBean
    private CategoryService categoryService;

    private static final String NAME_REQUIRED = "name is required";
    private static final String CATEGORY_REQUIRED = "category is required";

    @Test
    public void saveCategoryTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = post("/category/");
        when(categoryService.saveCategory(any(Category.class))).thenReturn(anyLong());
        controllerHelper.verifyBodyResults(invalidCategory(), status().isBadRequest(), $string(NAME_REQUIRED), validRequest);
        controllerHelper.verifyBodyResults(validCategory(), status().is2xxSuccessful(), $string(), validRequest);
    }

    @Test
    public void updateCategoryTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = put("/category/1");
        MockHttpServletRequestBuilder invalidRequest = put("/category/ ");
        controllerHelper.verifyBodyResults(invalidCategory(), status().isBadRequest(), $string(NAME_REQUIRED), validRequest);
        controllerHelper.verifyPathParametersResults(validCategory(), status().isBadRequest(), $string(CATEGORY_REQUIRED), invalidRequest);
        controllerHelper.verifyBodyResults(validCategory(), status().is2xxSuccessful(), $string(), validRequest);
    }

    @Test
    public void deleteCategoryTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = delete("/category/1");
        MockHttpServletRequestBuilder invalidRequest = delete("/category/ ");
        this.mvc.perform(validRequest).andExpect(status().isNoContent());
        this.mvc.perform(invalidRequest).andExpect(status().isBadRequest());
    }

    @Test
    public void getCategoryProducts() throws Exception {
        when(categoryService.getProducts(anyLong(), any(Currency.class))).thenReturn(Collections.emptySet());
        MockHttpServletRequestBuilder validRequest = get("/category/products/category/1/currency/USD");
        MockHttpServletRequestBuilder invalidCurrencyRequest = get("/category/products/category/1/currency/1");
        MockHttpServletRequestBuilder invalidCategoryRequest = get("/category/products/category/_/currency/USD");
        this.mvc.perform(validRequest).andExpect(status().isOk());

        MvcResult invalidCurrency = this.mvc.perform(invalidCurrencyRequest).andExpect(status().isBadRequest()).andReturn();
        assertThat("Invalid Currency", invalidCurrency.getResponse().getContentAsString(),
                containsString("Failed to convert value of type 'java.lang.String' to required type 'com.eshop.exchanges.Currency'"));

        MvcResult invalidCategory = this.mvc.perform(invalidCategoryRequest).andExpect(status().isBadRequest()).andReturn();
        assertThat("Invalid Category", invalidCategory.getResponse().getContentAsString(),
                containsString("Failed to convert value of type 'java.lang.String' to required type 'java.lang.Long'"));
    }

    @Test
    public void getCategoryTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = get("/category/1");
        MockHttpServletRequestBuilder invalidRequest = get("/category/% ");
        this.mvc.perform(validRequest).andExpect(status().isOk());
        this.mvc.perform(invalidRequest).andExpect(status().isBadRequest());
    }

    private static Category invalidCategory() {
        return new Category(1L, EMPTY, Collections.emptySet());
    }

    private static Category validCategory() {
        return new Category(1L, "Clothes", Collections.emptySet());
    }

    public static String[] $string(String... params) {
        return params;
    }
}
