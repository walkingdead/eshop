package com.eshop.unit.category.controller;

import com.eshop.category.CategoryController;
import com.eshop.category.CategoryService;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import static junitparams.JUnitParamsRunner.$;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(JUnitParamsRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerAuthorityTest {

    @MockBean
    private CategoryService userService;

    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @Before
    public void setup() throws Exception {
        mvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    @Parameters(method = "parametersForCategoryAuthorityTest")
    public void unauthorizedTest(MockHttpServletRequestBuilder builder) throws Exception {
        this.mvc.perform(builder)
                .andExpect(status().isUnauthorized());
    }

    public Object[] parametersForCategoryAuthorityTest() {
        return $(
                $(post("/category")),
                $(put("/category/1")),
                $(delete("/category/1")),
                $(get("/products/category/1/currency/1")),
                $(get("/1"))
        );
    }
}
