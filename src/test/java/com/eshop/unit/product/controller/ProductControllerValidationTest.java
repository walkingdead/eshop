package com.eshop.unit.product.controller;

import com.eshop.common.utils.ControllerTestHelper;
import com.eshop.exchanges.Currency;
import com.eshop.product.controller.ProductController;
import com.eshop.product.ProductDto;
import com.eshop.product.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ProductController.class)
@WithMockUser
public class ProductControllerValidationTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    private ControllerTestHelper controllerHelper;

    @Before
    public void setup() throws Exception {
        mvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
        controllerHelper = new ControllerTestHelper(mvc);
    }

    @MockBean
    private ProductService productService;

    private static final String NAME_REQUIRED = "product name is required";
    private static final String PRICE_REQUIRED = "product price is required";
    private static final String CATEGORY_ID_REQUIRED = "product category is required";
    private static final String CURRENCY_REQUIRED = "product currency is required";
    private static final String PRODUCT_ID_REQUIRED = "product id is required";

    @Test
    public void saveProductTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = post("/product/");
        when(productService.saveProduct(any(ProductDto.class))).thenReturn(anyLong());
        controllerHelper.verifyBodyResults(emptyProductDto(), status().isBadRequest(),
                $string(NAME_REQUIRED, PRICE_REQUIRED, CATEGORY_ID_REQUIRED, CURRENCY_REQUIRED), validRequest);

        controllerHelper.verifyBodyResults(productDtoWithName(), status().isBadRequest(),
                $string(PRICE_REQUIRED, CATEGORY_ID_REQUIRED, CURRENCY_REQUIRED), validRequest);

        controllerHelper.verifyBodyResults(productDtoWithNamePrice(), status().isBadRequest(),
                $string(CATEGORY_ID_REQUIRED, CURRENCY_REQUIRED), validRequest);

        controllerHelper.verifyBodyResults(productDtoWithNamePriceCategory(), status().isBadRequest(),
                $string(CURRENCY_REQUIRED), validRequest);

        controllerHelper.verifyBodyResults(fullProductDto(), status().isCreated(),
                $string(), validRequest);
    }

    @Test
    public void updateProductTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = put("/product/1");
        MockHttpServletRequestBuilder invalidRequest = put("/product/ ");
        controllerHelper.verifyBodyResults(emptyProductDto(), status().isBadRequest(),
                $string(NAME_REQUIRED, PRICE_REQUIRED, CATEGORY_ID_REQUIRED, CURRENCY_REQUIRED), validRequest);
        controllerHelper.verifyPathParametersResults(fullProductDto(), status().isBadRequest(), $string(PRODUCT_ID_REQUIRED),
                invalidRequest);
        controllerHelper.verifyBodyResults(fullProductDto(), status().is2xxSuccessful(), $string(), validRequest);
    }

    @Test
    public void deleteProductTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = delete("/product/category/1/product/1");
        MockHttpServletRequestBuilder invalidCategoryRequest = delete("/product/category/$/product/1");
        MockHttpServletRequestBuilder invalidProductRequest = delete("/product/category/1/product/%");
        this.mvc.perform(validRequest).andExpect(status().isNoContent());
        this.mvc.perform(invalidCategoryRequest).andExpect(status().isBadRequest());
        this.mvc.perform(invalidProductRequest).andExpect(status().isBadRequest());
    }

    @Test
    public void getProductTest() throws Exception {
        MockHttpServletRequestBuilder validRequest = get("/product/1/currency/USD");
        MockHttpServletRequestBuilder invalidCurrencyRequest = get("/product/1/currency/TTT");
        MockHttpServletRequestBuilder invalidProductRequest = get("/product/*/currency/AUD");
        this.mvc.perform(validRequest).andExpect(status().isNoContent());
        this.mvc.perform(invalidCurrencyRequest).andExpect(status().isBadRequest());
        this.mvc.perform(invalidProductRequest).andExpect(status().isBadRequest());
    }

    private static ProductDto emptyProductDto() {
        return new ProductDto();
    }

    private static ProductDto fullProductDto() {
        return new ProductDto("hat", BigDecimal.TEN, 1L, Currency.USD);
    }

    private static ProductDto productDtoWithName() {
        return new ProductDto("hat", null, null, null);
    }

    private static ProductDto productDtoWithNamePrice() {
        return new ProductDto("hat", BigDecimal.TEN, null, null);
    }

    private static ProductDto productDtoWithNamePriceCategory() {
        return new ProductDto("hat", BigDecimal.TEN, 1L, null);
    }

    public static String[] $string(String... params) {
        return params;
    }
}
