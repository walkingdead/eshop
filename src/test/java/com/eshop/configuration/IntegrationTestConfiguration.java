package com.eshop.configuration;


import com.eshop.configuration.security.SecurityConfig;
import com.eshop.exchanges.Currency;
import com.eshop.exchanges.CurrencyRate;
import com.eshop.exchanges.ExchangeService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Profile("test")
@ComponentScan(basePackages = "com.eshop")
@Import(value = {TestPersistenceConfiguration.class, EShopConfiguration.class, SecurityConfig.class})
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class IntegrationTestConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Qualifier("testExchangeService")
    public ExchangeService exchangeService() {
        return currency -> new CurrencyRate(Currency.USD, BigDecimal.valueOf(1.1));
    }

}
