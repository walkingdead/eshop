package com.eshop.common;


public final class ScriptConstants {

    private ScriptConstants() {
    }

    public static final String FILL_CATEGORY_SCRIPT = "classpath:/scripts/category/fill-category.sql";

    public static final String FILL_PRODUCT_SCRIPT = "classpath:/scripts/product/fill-product.sql";

}