package com.eshop.common.mappers.product;


import com.eshop.category.Category;
import com.eshop.common.metainfo.Category_;
import com.eshop.product.Product;
import com.eshop.common.metainfo.Product_;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;

public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet resultSet, int i) throws SQLException {

        Product product = new Product();
        product.setId(resultSet.getLong(Product_.PRODUCT_ID));
        product.setName(resultSet.getString(Product_.PRODUCT_NAME));
        product.setPrice(resultSet.getBigDecimal(Product_.PRODUCT_PRICE));
        Category category = new Category(resultSet.getLong(Category_.CATEGORY_ID),
                resultSet.getString(Category_.CATEGORY_NAME), Collections.emptySet());
        product.setCategory(category);
        return product;
    }
}
