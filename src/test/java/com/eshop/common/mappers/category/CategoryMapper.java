package com.eshop.common.mappers.category;


import com.eshop.category.Category;
import com.eshop.common.metainfo.Category_;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;


public class CategoryMapper implements RowMapper<Category> {

    @Override
    public Category mapRow(ResultSet resultSet, int i) throws SQLException {

        return new Category(resultSet.getLong(Category_.CATEGORY_ID),
                resultSet.getString(Category_.CATEGORY_NAME), Collections.emptySet());
    }
}
