package com.eshop.common.utils;


import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;

public class ControllerTestHelper {

    private static final Gson gson = new Gson();

    private final MockMvc mvc;

    public ControllerTestHelper(MockMvc mvc) {
        this.mvc = mvc;
    }

    public <T> void verifyBodyResults(T object, ResultMatcher status, String[] errorMessages,
                                      MockHttpServletRequestBuilder builder) throws Exception {

        String rawResponse = getResponse(object, status, builder);
        List<String> errors = new ArrayList<>();
        if (StringUtils.isNotBlank(rawResponse)) {
            errors = Arrays.asList(rawResponse.substring(rawResponse.indexOf('[') + 1, rawResponse.lastIndexOf(']')).split(", "));
        }
        assertThat(errors, hasItems(errorMessages));
    }

    public <T> void verifyPathParametersResults(T object, ResultMatcher status, String[] errorMessages,
                                                MockHttpServletRequestBuilder builder) throws Exception {
        String rawResponse = getResponse(object, status, builder);
        List<String> errors = new ArrayList<>();
        if (StringUtils.isNotBlank(rawResponse)) {
            JsonElement response = new JsonParser().parse(rawResponse);
            JsonElement message = response.getAsJsonObject().get("message");
            errors = Collections.singletonList(message.getAsString());
        }
        assertThat(errors, hasItems(errorMessages));
    }

    private <T> String getResponse(T object, ResultMatcher status,
                                   MockHttpServletRequestBuilder builder) throws Exception {
        MvcResult result = this.mvc.perform(builder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(gson.toJson(object)))
                .andExpect(status)
                .andReturn();
        return result.getResponse().getContentAsString();
    }
}
