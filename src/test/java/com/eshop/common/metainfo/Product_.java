package com.eshop.common.metainfo;

public class Product_ {

    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String PRODUCT_NAME = "PRODUCT_NAME";
    public static final String PRODUCT_PRICE = "PRODUCT_PRICE";
    public static final String PRODUCT_CATEGORY = "PRODUCT_CATEGORY";
}
